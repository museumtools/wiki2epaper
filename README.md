# Scripts to interface MuseumTools Wiki via API

http://museumtools.artserver.org/wiki

## Requirements
* [mwclient](https://mwclient.readthedocs.io/en/latest/index.html) 
* [pyYaml](https://github.com/yaml/pyyaml.org)
* [Jinja2](https://jinja.palletsprojects.com)
* [WeasyPrint](https://weasyprint.readthedocs.io)
* [Pandoc](https://pandoc.org/)
* [imagemagick](https://imagemagick.org/index.php)

`pip install -r requirements.txt`

`apt install pandoc imagemagick`



## RUN

`python wikiAPI/ask_artifact_info.py`

### Run Weasy Print Navigator
`python -m weasyprint.tools.navigator`