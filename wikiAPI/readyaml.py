import yaml
from pathlib import Path
from mwclient import Site

pwd = Path(__file__)
project_dir = pwd.parent.absolute()
details_path = project_dir / 'datatypes.yml'

with open(details_path, 'r') as details_f:
    details = yaml.load(details_f.read(), Loader=yaml.BaseLoader)
print(details)