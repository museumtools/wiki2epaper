#!/bin/sh

# Device Accepted Image : test_mono.png PNG 1404x1872 1404x1872+0+0 8-bit Gray 2c 15915B 0.000u 0:00.000
# 8bit, 2c (colors) is essential

# PDF TO PNG: resize

convert -quality 300 -density 150 ../output_pdfs/artifacts.pdf \
-background white -alpha remove \
-resize 1404x1872! \
bitmap_%02d.png

# COLOR PNG to 2bit PNG
convert  bitmap_00.png \
-colorspace Gray -depth 2 \
-ordered-dither h4x4a \
bw_bitmaps_00.png



convert -quality 300 -density 150 ../output_pdfs/artifacts.pdf \
-background white -alpha remove \
-resize 1200x1600! \
bitmap_%02d.png

# Not is use

convert -depth 1 -quality 300 -density 150 ../output_pdfs/artifacts.pdf \
-ordered-dither h4x4a \
-resize 1404x1872! \
dither_%02d.png



# -resize 1404x1872!   FOR 7.8"


# 2 colors == -depth 1

# -channel all -colorspace Gray -random-threshold  0x100% -separate random_%02d.jpg


# See Section: Ordered Dither using Uniform Color Levels
# in http://www.imagemagick.org/Usage/quantize/#od_posterize

# h4x4a \
# o8x8,32,64,32 \
#  \