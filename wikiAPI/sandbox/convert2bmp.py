import shlex
import subprocess
import yaml
from pathlib import Path
from PIL import Image, ImageEnhance

current_dir = Path(__file__).parent.absolute()
details_path = current_dir / 'display_details.yml'
images_dir = current_dir  # / 'images-Jon'
# read details from display_details.yml
with open(details_path, 'r') as details_f:
    device_details = yaml.load(details_f.read(), Loader=yaml.BaseLoader)
device = device_details['13']
current_dir = Path(__file__).parent.absolute()
image_src = current_dir / 'sourceimg.jpg'
image_dest = current_dir / 'destimg.data'

dimensions = device.get('dimensions')
resize_cmd = f'mogrify -resize {dimensions}^ -gravity center -extent ' \
             f'{dimensions} {image_src}'
subprocess.call(shlex.split(resize_cmd))

with Image.open(image_src) as im:
    brightness = ImageEnhance.Brightness(im)
    im = brightness.enhance(2)
    # im = im.resize((1404, 1872))  # # TODO: crop co
    blackAndWhiteImage = im.convert("1")
    # blackAndWhiteImage.show()
    quantizedImage = im.quantize(colors=2, method=2)
    # print(quantizedImage.format, quantizedImage.size, blackAndWhiteImage.mode)
    blackAndWhiteImage.save(image_dest, "BMP")

#im.show()