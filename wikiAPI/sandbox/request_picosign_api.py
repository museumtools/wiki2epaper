import requests
import yaml
import base64
import json
from typing import Dict
from pathlib import Path
from random import randint

current_dir = Path(__file__).parent.absolute()
details_path = current_dir / 'display_details.yml'
images_dir = current_dir  # / 'images-Jon'
# read details from display_details.yml
with open(details_path, 'r') as details_f:
    device_details = yaml.load(details_f.read(), Loader=yaml.BaseLoader)
device = device_details['13']
print(device)

def device_info(id_: str, bearer: str) -> Dict:
    # get device info
    response = requests.get(
        url=f'https://new.mypicosign.com/api/info/{id_}',
        headers={
            'Accept': 'application/json',
            'Authorization': f'Bearer {bearer}'
        })
    return response


info = device_info(id_=device["device_id"], bearer=device["bearer"])
print(info.json())


def device_post_img(id_: str, bearer: str, imgfn: str) -> object:
    with open(imgfn, 'rb') as img:
        # base64.b64encode returns a bytes instance,
        # so it's necessary to call decode to get a str
        img_b64 = base64.b64encode(img.read()).decode()
    response = requests.post(
        url=f'https://new.mypicosign.com/api/image/'
            f'{id_}?display=0&singleupdate=false&flashless=false',
        headers={'Content-type': 'application/json', 'Accept': 'application/json',
                 'Authorization': f'Bearer {bearer}'},
        data=json.dumps({'image': f'data:image/png;base64,{img_b64}'})
    )
    return response

post_response = device_post_img(
    id_=device["device_id"],
    bearer=device["bearer"],
    imgfn=f'{images_dir}/destimg.data')  # {str(randint(1,3))}.data
print(type(post_response))
print(post_response.json())


##########
# ISSUE: with uploading with PUT, hence using POST method
#########
def device_imgfile_upload(id_: str, bearer: str, imgfn: str) -> Dict:
    print(imgfn)
    with open(imgfn, 'rb') as img:
        print(img)
        response = requests.put(
            url=f'https://new.mypicosign.com/api/image/{id_}?'
                f'display=0&process=false&singleupdate=false&nodither=false&'
                f'whitebg=false&flashless=false',
            data=img.read(),
            headers={
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json',
                'Authorization': f'Bearer {bearer}'
        })
    return response  # .status_code, response.json()

# response = device_imgfile_upload(
#     id_=device["device_id"],
#     bearer=device["bearer"],
#     imgfn=f'{images_dir}/{str(randint(1,3))}.data')
#
# print(response)


