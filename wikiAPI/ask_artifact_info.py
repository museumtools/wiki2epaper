import argparse
import yaml
import subprocess
import os
import shlex
from pathlib import Path
from datetime import datetime
from _collections import OrderedDict
from jinja2 import (FileSystemLoader,
                    Environment)
from pprint import pprint
from weasyprint import HTML, CSS
from login.login import site

p = argparse.ArgumentParser(
    description="Wiki ask query to bitmaps images.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
p.add_argument('-d', '--device', choices=["7.8", "13"], default="7.8",
               help='optimized bitmaps for which device?')

args = p.parse_args()

pwd = Path(__file__)
project_dir = pwd.parent.absolute()
dir_html = 'output_htmls'
dir_pdf = 'output_pdfs'
fn_pdf = 'artifacts.pdf'
dir_pdf_imgs = 'output_bitmaps'

base_template = env.get_template('base.html.jinja')

# Datatype yaml file
datatypes_path = project_dir / 'datatypes.yml'
with open(datatypes_path, 'r') as datatypes_path_f:
    datatypes = yaml.load(datatypes_path_f.read(), Loader=yaml.BaseLoader)
# print(datatypes)


def get_prop_datatype(prop):
    datatype_q = site.ask(f'[[Property:{prop}]]|?Has type|mainlabel=-')
    datatype_list = list(datatype_q)
    datatype_url = datatype_list[0]['printouts']['Has type'][0]
    datatype = datatypes[datatype_url.split('#')[-1]]
    return datatype


def parse_artifact(ask_resp_dict):
    artifact_page = ask_resp_dict['fulltext']  # TO TEMPLATE
    # print('*' * 30, '\n', artifact_page, '\n', '*' * 30)
    metadata = ask_resp_dict['printouts']
    artifact_creator = metadata['Creator'][0]  # TO TEMPLATE
    img_page = (metadata['Has image'][0]['fulltext']).replace('File:','')
    img = site.images[img_page]
    img_url = img.imageinfo.get('url')
    return artifact_page, artifact_creator, img_url


def parse_description(ask_resp):
    # by looking at the property dataype function returns
    # a list of OrderDicts w. the following structure
    # [ {"Page": "Pagename",
    #   "Desction Text": ["jowq", "ojorwer"],
    #   "Date": [datetime.datetime(2020, 5, 5, 2, 0)]
    #   },
    #   {...}]
    # w/ the exception of Page all values are in list, as there can more than 1
    # value per property
    desc_dict_list = []
    for row in ask_resp:
        desc_dict = OrderedDict()
        desc_dict['Page'] = row['fulltext']
        for k, v in row['printouts'].items():
            desc_dict[k] = []
            for entry in row['printouts'][k]:
                prop_datatype = get_prop_datatype(prop=k)
                if prop_datatype == 'Date':
                    desc_dict[k].append(
                        datetime.fromtimestamp(int(entry['timestamp']))
                    )
                elif prop_datatype == 'Page':
                    desc_dict[k].append(entry['fulltext'])
                elif prop_datatype == 'Text':
                    # import pdb; pdb.set_trace()
                    desc_dict[k].append(pandoc(pwd=project_dir,
                                               content=entry,
                                               format_in='mediawiki',
                                               format_out='HTML'))
                else:
                    desc_dict[k].append(entry)
        desc_dict_list.append(desc_dict)
    return desc_dict_list


def pandoc(pwd, content, format_in, format_out):
    # print('HTML content file:', wiki_content_f.name)
    # tmp files
    mw_tmp_fn = pwd / '.mediawiki_content'
    html_tmp_fn = pwd / '.html_content'
    for fn in [mw_tmp_fn, html_tmp_fn]:
        if os.path.isfile(fn) is False:
            os.mknod(fn) # create them if not in dir
    with open(mw_tmp_fn, 'w') as mw_tmp_fn_:
        mw_tmp_fn_.write(content)

    pandoc_cmd = f"pandoc {mw_tmp_fn} -f {format_in} -t {format_out} -o {html_tmp_fn}"
    subprocess.call(shlex.split(pandoc_cmd))

    with open(html_tmp_fn, 'r') as html_tmp_fn_:
        output = html_tmp_fn_.read()

    return output


def create_pdf(pdfdir, fn_pdf, html_code, css_file, css_page_file):
    # give HTML and CSS to Weasyprint to create PDF
    if os.path.isdir(pdfdir) is False:
        os.mkdir(path=pdfdir, mode=0o777)  # create them if not in dir
    html = HTML(string=html_code)
    html.write_pdf(target=f'{pdfdir}/{fn_pdf}',
                   stylesheets=[CSS(filename=css_file),
                                CSS(filename=css_page_file)])


def pdf2bitmap(pdf, imgdir, device):
    if os.path.isdir(imgdir) is False:
        os.mkdir(path=imgdir, mode=0o777)  # create them if not in dir
    #  -density sets the horizontal and vertical resolution of an image
    # -depth number of bits in a color sample within a pixel
    if device == '7.8':
        size = '1404x1872'
    elif device == '13':
        size = '1600x1200'
    print(device, size)
    convert_cmd = f'convert -quality 300 -density 150 "{pdf}" ' \
                  f'-background white -alpha remove -resize {size}!' \
                  f' {imgdir}/_rand_%02d.png'
    subprocess.call(shlex.split(convert_cmd))

    # dithers = [
    #     f'-colorspace GRAY -ordered-dither h4x4a,4 {imgdir}/_h4x4a_%02d.jpg',
    #     f'-colorspace GRAY -ordered-dither checks,4 {imgdir}/_checks%02d.jpg',
    #     f'-channel B -colorspace GRAY -random-threshold 0x100% -separate'
    #     f' {imgdir}/_rand_%02d.pdf'
    # ]

    for bitmap in os.listdir(imgdir):
        print(bitmap)
        mogrify_cmd = f'mogrify -colorspace Gray -depth 2 ' \
                      f'-ordered-dither h4x4a ' \
                      f'{imgdir}/{bitmap}'
        subprocess.call(shlex.split(mogrify_cmd))



# query Artifact objects which include description and Image
q_artifacts = "[[Category:Artifact]][[Description::+]][[Has image::+]]" \
              "|?Title|?Creator|?Date|?Source|?Has image"

artifacts_all_html = ''
for artifact in site.ask(query=q_artifacts):
    a_page, a_creator, a_img = parse_artifact(artifact)

    # query Description objects
    q_desc = f"[[Category:Description]][[Source::{a_page}]]" \
             f"|?Description Text|?Creator|?Date|?Language|?Source" \
             f"|sort=Date|order=desc"

    descriptions = parse_description(site.ask(q_desc))
    pprint(descriptions)
    artifacts_all_html += artifact_template.render(d=a_page,
                                                   a_title=a_page,
                                                   a_img_src=a_img,
                                                   descs=descriptions)

artifact_page = base_template.render(
    title="Artifacts",
    stylesheet="../static/style.css",
    body=artifacts_all_html
)

if os.path.isdir(f'{project_dir}/{dir_html}') is False:
    os.mkdir(path=f'{project_dir}/{dir_html}', mode=0o777)
with open(f'{project_dir}/{dir_html}/artifacts.html', 'w') as artifact_f:
    artifact_f.write(artifact_page)

if args.device == "7.8":
    page_css_file = "page_78inch.css"
elif args.device == "13":
    page_css_file = "page_13inch.css"

create_pdf(pdfdir=f'{project_dir}/{dir_pdf}',
           fn_pdf=fn_pdf,
           html_code=artifact_page,
           css_file=f'{project_dir}/static/style.css',
           css_page_file=f'{project_dir}/static/{page_css_file}')

pdf2bitmap(pdf=f'{project_dir}/{dir_pdf}/{fn_pdf}',
           imgdir=f'{project_dir}/{dir_pdf_imgs}',
           device=args.device)
