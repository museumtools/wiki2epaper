import argparse
import yaml
from pathlib import Path
from pprint import pprint
from login.login import site
import re

def subject_properties(subject):
    response = site.raw_api('browsebysubject', subject=subject,
                            http_method='GET')
    prop_val_dict = {}
    for prop_val in response['query']['data']:
        if 'dataitem' in prop_val.keys():
            prop = prop_val['property']
            if prop not in prop_val_dict.keys():
                prop_val_dict[prop] = []
            for dataitem in prop_val['dataitem']:
                val = str(dataitem['item'])
                val = re.sub(pattern=r"#\d+?##", repl="", string=val)
                val_type = dataitem['type']
                prop_val_dict[prop].append({"val_type": val_type,
                                            "val": val})
    return prop_val_dict


def parse_value_page(prop: str, pagename: str):
    if prop == 'Description':
        desc_page = site.Pages[desc_value]
        desc_dict = subject_properties(subject=desc_value)  # subject properties
        # print('DESC PAGE:', desc_page)
        # print('DESC PAGE TEXT:', desc_page.text())
        # pprint(desc_dict)
        return desc_dict


artifact = site.Categories['Artifact']
for cat in artifact.members():
    if 'test' not in cat.page_title.lower():
        print(cat.namespace, cat.page_title, cat.pageid)
        text = cat.text()
        page_propvals_dict = subject_properties(subject=cat.page_title)
        if 'Description' in page_propvals_dict.keys():
            for desc_dict in page_propvals_dict['Description']:
                desc_type = desc_dict['val_type']
                desc_value = desc_dict['val']
                print(desc_value)
                desc_dict = parse_value_page(prop='Description',
                                             pagename=desc_value)
                desc_text = desc_dict.get('Description_Text')[0].get('val')

                print('*' * 20)
                print('DESC TEXT:', desc_text)
                print('*' * 20, '\n')

            # http://museumtools.artserver.org/wiki/api.php?action=browsebysubject&subject=Test%20Artifact&format=json
